import React, {Component} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  FlatList, 
  Text, 
  View,
  TextInput,
  Image,
  Button,
  TouchableOpacity, 
  Alert,
 } from 'react-native';

import OurFlatList from './app/components/ourFlatList/OurFlatList';
import ConexionFetch from './app/components/conexionFetch/ConexionFetch';

import TransferenceFirst from './app/components/transferences/TransferenceFirst';
import TransferenceSecond from './app/components/transferences/TransferenceSecond';
import TransferenceThird from './app/components/transferences/TransferenceThird';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

//import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
//import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

const Stack = createStackNavigator();
//const Tab = createBottomTabNavigator();
const Tab = createMaterialBottomTabNavigator();
//const Tab = createMaterialTopTabNavigator();

const TransferenceStack = createStackNavigator();

function TransferenceStackScreen() {
  return(
    <TransferenceStack.Navigator>
      <TransferenceStack.Screen name="First" component={TransferenceFirst} options={{
          title: 'Defina los datos', headerStyle: {  backgroundColor: '#98FB98',}, headerTintColor: '#fff',
          headerTitleStyle: {fontWeight: 'bold',}, }}/>
      <TransferenceStack.Screen name="Second" component={TransferenceSecond} options={{
          title: 'Confirme datos', headerStyle: {  backgroundColor: '#98FB98',}, headerTintColor: '#fff',
          headerTitleStyle: {fontWeight: 'bold',}, }}/>
      <TransferenceStack.Screen name="Third" component={TransferenceThird} options={{
          title: 'Transaccion finalizada', headerStyle: {  backgroundColor: '#98FB98',}, headerTintColor: '#fff',
          headerTitleStyle: {fontWeight: 'bold',}, }}/>
    </TransferenceStack.Navigator>
  )
}

function Item({ title,pic }) {
  return (
    <View style={styles.item}>
      <Image style={styles.listImage} resizeMode='cover' source={{uri: pic}}/>
      <Text style={styles.listText}>{title}</Text>
    </View>
  );
}

function HomeScreen ({navigation}) {
  return(
    <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Screen</Text>
      <Button
        title="Go to Details"
        onPress={() =>navigation.navigate('Details')}
        />
    </View>
  );
}

function DetailsScreen ({navigation}) {
  return(
    <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Details Screen</Text>

      <Button
        title="Go back to first screen in stack"
        onPress={() =>navigation.navigate('Home')}
        />
    </View>
  );
}



export default class App extends Component {
  constructor (props){
    super(props);
    this.state ={
      texto: "",
      textValue: '',
      count: 0,
    };
  }

  changeTexto = text => {
    this.setState ({texto: text>18 ? "es mayor" : "es menor"});
    //if (this.state.texto[0]>18) this.state.texto[0]= "es mayor"
    //else this.state.texto[0]="es menor"
  }

  textoInvalido = text =>{
    this.setState ({texto: "SOLO NUMEROS!!"})
  }

  changeTextInput = text => {
    this.setState({textValue: text});
  };

  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  showAlert = () => {
    Alert.alert(
      'Titulo',
      'Mensaje',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  };

  render() {
    return (
    <NavigationContainer>
        <Tab.Navigator
        initialRouteName = "Home"
        tabBarOptions={{
          activeTintColor: '#e91e63',
        }}>
          <Tab.Screen 
          name="Home" 
          component={HomeScreen} 
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({color, size}) => (<MaterialCommunityIcons name="home" color={color} size={24} />),
          }}
          />
          <Tab.Screen 
          name="Transferences" 
          component={TransferenceStackScreen} 
          options={{
            tabBarLabel: 'Transferences',
            tabBarIcon: ({color, size}) => (<MaterialCommunityIcons name="cash" color={color} size={24} />),
          }}
          />
          <Tab.Screen 
          name="Details" 
          component={DetailsScreen} 
          options={{
            tabBarLabel: 'Details',
            tabBarIcon: ({color, size}) => (<MaterialCommunityIcons name="bell" color={color} size={24} />),
            //tabBarIcon: "biohazard"
            
          }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
}

/* Esto va dentro de nagigationcontainer dentro de render
<Stack.Navigator>
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Details" component={DetailsScreen} />
        </Stack.Navigator>
*/

const styles = StyleSheet.create({ 
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10
  },
  item: {
    flexDirection:'row',
    backgroundColor: '#454588',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16
  },
  listImage:{
    width: 96,
    height: 96
  },
  listText: {
    fontSize: 30
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },

  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
});
